��          |      �             !     1     B  -   X  /   �     �     �     �     �     �  !     �  5     �     �     �        &   ,     S  *   b     �     �     �  8   �                                 	          
                    Date: $date     From: $from_     Subject: $subject $mlist.display_name subscription notification $mlist.display_name unsubscription notification (no subject) Message has no subject Original Message Today's Topics:
 Unsubscription request list:admin:notice:unsubscribe.txt Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-09 00:41+0000
Last-Translator: Shohei Kusakata <shohei@kusakata.com>
Language-Team: Japanese <https://hosted.weblate.org/projects/gnu-mailman/mailman/ja/>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.1-dev
     日付: $date     送信元: $from_     題名: $subject $mlist.display_name 購読通知 $mlist.display_name 購読解除通知 (題名なし) メッセージに題名がありません 元のメッセージ 今日のトピック:
 購読解除リクエスト $member は $display_name から削除されました。 
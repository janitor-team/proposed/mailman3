��          �      �       0  K   1  -   }     �     �  3   �     �               $  ;   ;  D   w  J   �  �    U   �  4   �     $     2  O   H     �     �     �  &   �  D   �  B   0  R   s                                	                   
       $member unsubscribed from ${mlist.display_name} mailing list due to bounces $mlist.display_name subscription notification (no subject) Accept a message. Display more debugging information to the log file. Moderation chain N/A Original Message Unsubscription request Welcome to the "$mlist.display_name" mailing list${digmode} You have been unsubscribed from the $mlist.display_name mailing list Your subscription for ${mlist.display_name} mailing list has been disabled Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-12 23:43+0000
Last-Translator: ssantos <ssantos@web.de>
Language-Team: Portuguese <https://hosted.weblate.org/projects/gnu-mailman/mailman/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.3-dev
 $member desubscrevido da lista de discussão de ${mlist.display_name} devido a saltos Notificação da subscrição de $mlist.display_name (sem assunto) Aceitar uma mensagem. Exibir mais informação de depuração para o ficheiros de registo de eventos. Cadeia de moderação N/D Mensagem Original Pedido de cancelamento de subscrição Bem-vindo à lista de endereços "$mlist.display_name" de ${digmode} Cancelou a subscrição da lista de endereços $mlist.display_name A sua subscrição para a lista de discussão ${mlist.display_name} foi desativada 